Gitlab Runners
==============

Podman Gitlab Runner
====================


.. image:: https://readthedocs.org/projects/runners/badge/?version=latest
    :alt: Documentation Status
    :target: https://runners.readthedocs.io/en/latest/?badge=latest

.. image:: https://img.shields.io/docker/image-size/adamkoro/podman-gitlab-runner/latest   
    :alt: Docker Image Size (latest)
    :target: https://hub.docker.com/r/adamkoro/podman-gitlab-runner

.. image:: https://img.shields.io/docker/pulls/adamkoro/podman-gitlab-runner   
    :alt: Docker Pulls

.. image:: https://img.shields.io/docker/stars/adamkoro/podman-gitlab-runner
    :alt: Docker Stars

.. image:: https://img.shields.io/docker/v/adamkoro/podman-gitlab-runner?arch=amd64   
    :alt: Docker Image Version (latest by date)

First you need to run: - I am using volume to store gitlab runner config, if u want to can mount a simple directory. You have to mount to this point: "/etc/gitlab-runner"

- Mount volume: -v podman-gitlab-runner-1:/etc/gitlab-runner
- Mount directory: -v /opt/gitlab-runner:/etc/gitlab-runner
- Gitlab executor always have to set to shell! - **DO NOT CHANGE IT!**

podman run -d --privileged --name CONTAINER_NAME -v PODMAN_SOCKET:/run/podman/podman.sock -v YOUR_VOLUME_or_DIR:/etc/gitlab-runner adamkoro/podman-gitlab-runner:latest

Register to Gitlab:

podman exec -it CONTAINER_NAME gitlab-runner register -n --url GIT_URL --registration-token=YOUR_TOKEN --executor shell --description "YOUR_DESC" --tag-list "YOUR_TAGS"

Implement in CD/CD

When you are implemeting in CI/CD, you have to set **podman-remote** instead of podman in "script" section.

Docker Gitlab Runner
====================
.. image:: https://readthedocs.org/projects/runners/badge/?version=latest
    :alt: Documentation Status
    :target: https://runners.readthedocs.io/en/latest/?badge=latest


.. image:: https://img.shields.io/docker/image-size/adamkoro/docker-gitlab-runner/latest   
    :alt: Docker Image Size (latest)
    :target: https://hub.docker.com/r/adamkoro/docker-gitlab-runner

.. image:: https://img.shields.io/docker/pulls/adamkoro/docker-gitlab-runner   
    :alt: Docker Pulls

.. image:: https://img.shields.io/docker/stars/adamkoro/docker-gitlab-runner
    :alt: Docker Stars

.. image:: https://img.shields.io/docker/v/adamkoro/docker-gitlab-runner?arch=amd64   
    :alt: Docker Image Version (latest by date)