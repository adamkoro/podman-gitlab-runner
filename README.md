# Gitlab Runners

## Podman Gitlab Runner

[![Documentation Status](https://readthedocs.org/projects/runners/badge/?version=latest)](https://runners.readthedocs.io/en/latest/?badge=latest)
[![Gitlab pipeline status](https://gitlab.com/adamkoro-group/runners/badges/main/pipeline.svg)](https://gitlab.com/adamkoro-group/runners/-/commits/main)
[![Docker Image Size(latest)](https://img.shields.io/docker/image-size/adamkoro/podman-gitlab-runner/latest)](https://hub.docker.com/r/adamkoro/podman-gitlab-runner)
[![Docker Pulls](https://img.shields.io/docker/pulls/adamkoro/podman-gitlab-runner)]()
[![Docker Stars](https://img.shields.io/docker/stars/adamkoro/podman-gitlab-runner)]()
[![Docker Image Version (latest by date)](https://img.shields.io/docker/v/adamkoro/podman-gitlab-runner?arch=amd64)]()

---

## Docker Gitlab Runner

[![Documentation Status](https://readthedocs.org/projects/runners/badge/?version=latest)](https://runners.readthedocs.io/en/latest/?badge=latest)
[![Gitlab pipeline status](https://gitlab.com/adamkoro-group/runners/badges/main/pipeline.svg)](https://gitlab.com/adamkoro-group/runners/-/commits/main)
[![Docker Image Size(latest)](https://img.shields.io/docker/image-size/adamkoro/docker-gitlab-runner/latest)](https://hub.docker.com/r/adamkoro/docker-gitlab-runner)
[![Docker Pulls](https://img.shields.io/docker/pulls/adamkoro/docker-gitlab-runner)]()
[![Docker Stars](https://img.shields.io/docker/stars/adamkoro/docker-gitlab-runner)]()
[![Docker Image Version (latest by date)](https://img.shields.io/docker/v/adamkoro/docker-gitlab-runner?arch=amd64)]()

---

### Check out documentation by the following [link](https://runners.readthedocs.io/en/latest/).
